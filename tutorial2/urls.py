from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("form.urls")),
    path('accounts/', include("accounts.urls")),
    path('upload/', include('upload.urls')),
]
