import os
from dotenv import load_dotenv
from django.conf import settings
from django.core.wsgi import get_wsgi_application
load_dotenv(os.path.join(settings.BASE_DIR, '.env'))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tutorial2.settings')
application = get_wsgi_application()
