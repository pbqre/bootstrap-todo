from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.conf import settings

def get_user(requests):
  return HttpResponse(f'Hello {settings.EMAIL_HOST_USER}  {settings.EMAIL_HOST_PASSWORD}')
def register_user(requests):
  context = {
    'form': UserCreationForm()
  }
  if requests.method == 'POST':
    form = UserCreationForm(requests.POST or None)
    if form.is_valid():
      form.save()
      return redirect('home')
    return HttpResponse(form.errors)
  return render(requests, 'registration/register.html', context=context)