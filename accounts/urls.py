from django.urls import path, include
from accounts import views
from django.contrib.auth import urls

urlpatterns = [
  # Add new paths here...
  path('', include('django.contrib.auth.urls')),
  path('register', views.register_user, name='register-user'),
  path('get', views.get_user)
]