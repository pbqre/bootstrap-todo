from django.db import models
from django.db.models.base import Model

# Create your models here.
class Task(models.Model):
  """ A simple model to store task. """
  text = models.CharField(max_length=100, unique=True, default="A default Task")
  is_completed = models.BooleanField(default=False)

  def __str__(self):
    """
    This method will help the django-admin to display 
    the field.
    """
    return f"{self.pk} {self.text}"

"""
 [ modal ]
     |  -> makemigrations
 [ migrations ]
    |  -> Migrate
 [ Database Changed ]
"""

class School(models.Model):
  """
  - OneToOne
  - ManyToOne # ForiegnKey
  - ManyToMany
  """
  LOCATION = (
    ('Delhi', 'delhi'),
    ('Mumbai', 'munbai'),
    ('Agra', 'agra')
  )
  name = models.CharField(max_length=30)
  location = models.CharField(choices=LOCATION, max_length=100)

  def __str__(self):
    return self.name


class Person(models.Model):
  # Start writing fields here ...
  # primary = models.AutoField()
  name = models.CharField(max_length=150)
  # age = models.IntegerField(default=0)
  # age = models.PositiveIntegerField()
  school = models.ForeignKey(School, on_delete=models.SET_NULL, null=True)
  # on_delete :: models.CASCADE || models.SET_NULL
  is_educated = models.BooleanField(null=True)

  def __str__(self):
    return self.name


# =============== ManyToMany Relation ============

class Book(models.Model):
  name = models.CharField(max_length=150)
  price = models.DecimalField(decimal_places=2, max_digits=5)

  def __str__(self):
    return self.name

class Author(models.Model):
  books = models.ManyToManyField(Book, related_name='reverse')
  name = models.CharField(max_length=150)

  @property
  def contains_book(self):
    if self.books.all():
      return True
    else:
      return False

  def __str__(self):
    return self.name

class A(models.Model):
  name = models.CharField(max_length=20)
  
  def __str__(self):
    return self.name

class B(models.Model):
  name = models.CharField(max_length=20)
  a = models.OneToOneField(A, on_delete=models.CASCADE)

  def __str__(self):
    return self.name
