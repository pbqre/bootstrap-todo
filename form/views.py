from django.shortcuts import render, HttpResponse, redirect
from form.models import Task
from form.forms import LoginForm, TaskForm, TaskModelForm
# Create your views here.

def update_task(requests, id):
  try:
    task = Task.objects.get(id=id)
  except Task.DoesNotExist:
    return HttpResponse('Does Not Exist.')
  
  t = TaskModelForm(instance=task)
  completed = False
  if task.is_completed:
    completed = True

  if requests.method == 'POST':
    t = TaskModelForm(instance=task, data=requests.POST or None)
    if t.is_valid():
      form = t.save(commit=False)
      if not form.is_completed and completed:
        form.is_completed = True
      form.save()
      return redirect('home')

  context = {
    'form': t,
    'task': task
  }

  return render(requests, 'update-task.html', context=context)


# def update_task(request, id):
#   try:
#     task = Task.objects.get(id=id)
#   except Task.DoesNotExist:
#     return HttpResponse('Does Not Exist.')
  
#   context = {
#     'task': task
#   }
#   if request.method == 'POST':
#     text = request.POST.get('text')
#     completed = request.POST.get('isCompleted')
#     if text:
#       is_completed = False
#       if completed == 'on':
#         is_completed = True
    
#     task.text = text
#     task.is_completed = is_completed
#     task.save()
#     return redirect('home')
#   return render(request, 'update-task.html', context=context)


def delete_task(request, id):
  try:
    task = Task.objects.get(id=id)
  except Task.DoesNotExist:
    return HttpResponse('Does Not Exist.')
  
  task.delete()
  return redirect('home')


def display_task(request, id):
  if not request.user.is_authenticated:
    return HttpResponse("Naah, You are an alien.")

  try:
    task = Task.objects.get(id=id)
  except Task.DoesNotExist:
    return HttpResponse('Does Not Exist.')
  context = {
    'task': task
  }
  return render(request, 'display-task.html', context=context)
  # return HttpResponse("Current Id: " + str(id))

def login_view(requests):
  if requests.method == 'POST':
    # Get the Form.
    t = TaskForm(requests.POST or None)

    if t.is_valid():
      task = t.cleaned_data['task']
      completed = t.cleaned_data['completed']
      Task.objects.create(text=task, is_completed=completed)
      return redirect('home')
    else:
      return HttpResponse(t.errors)
  
  


def home(request):
  context = {
    'tasks': Task.objects.all(),
    'form': TaskForm()
  }
  if request.method == 'POST':
    text = request.POST.get('task')
    completed = request.POST.get('completed')
    if text:
      is_completed = False
      if completed == 'on':
        is_completed = True
      
      # Creating the task.
      task = Task(text=text, is_completed=is_completed)
      task.save()
      
    print(text, completed, "\n\n\n\n")
  return render(request, 'home.html', context=context)