# Generated by Django 3.2.2 on 2021-05-13 05:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0010_alter_person_school'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('price', models.DecimalField(decimal_places=2, max_digits=5)),
            ],
        ),
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('books', models.ManyToManyField(to='form.Book')),
            ],
        ),
    ]
