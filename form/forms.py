from django.forms import fields
import form
from django import forms
from form.models import Task

class TaskForm(forms.Form):
	task = forms.CharField(
			label='Enter the task to add', 
			required=True, 
			max_length=100, 
			min_length=10,
		)
	completed = forms.BooleanField(label='Completed', required=True)



class TaskModelForm(forms.ModelForm):
	class Meta:
		model = Task
		# fields = [
		# 	'text',
		# 	'is_completed'
		# ]k
		fields = '__all__'

class LoginForm(forms.Form):
	username = forms.CharField(
		label="Enter Username", 
		required=True, 
		max_length=100,
		widget=forms.TextInput(attrs={
			'class': 'form-control'
		})
	)
	password = forms.CharField(
			label="Enter Password", 
			required=True, 
			max_length=100, 
			widget=forms.PasswordInput(attrs={
				'class': 'form-control'
			})
		)
	saved = forms.BooleanField(
			label="save", 
			required=True,
			widget=forms.CheckboxInput(attrs={
				'class': 'form-check-input'
			})
		)
