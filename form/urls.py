from django.urls import path
from form import views

urlpatterns = [
  path('', views.home, name="home"),
  path('task/<int:id>', views.display_task, name="task"),
  path('task/<int:id>/delete', views.delete_task, name="delete-task"),
  path('task/<int:id>/update', views.update_task, name="update-task"),
  path('login/', views.login_view, name='login-view')
]