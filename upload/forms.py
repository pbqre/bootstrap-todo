from django import forms
from django.contrib.auth.models import User
from django.forms import fields
from upload.models import UserResume

class UserResumeForm(forms.ModelForm):
  class Meta:
    model = UserResume
    fields = ['resume']