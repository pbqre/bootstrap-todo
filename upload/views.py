from django.shortcuts import render, HttpResponse
from upload.models import UserResume
from upload.forms import UserResumeForm
from django.contrib.auth.decorators import login_required


@login_required
def upload_resume(request):
  context = {
    'form': UserResumeForm()
  }
  if request.method == 'POST':
    form = UserResumeForm(request.POST or None, request.FILES)
    if form.is_valid():
      obj = form.save(commit=False)
      obj.user = request.user
      obj.save()

    else:
      return HttpResponse('Error in the upload form.')
  return render(request, 'upload/resume.html', context=context)