from django.db import models
from django.contrib.auth.models import User

class UserResume(models.Model):
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  # resume = models.FileField(upload_to="resumes/")
  resume = models.ImageField(upload_to="resumes/")
  def __str__(self):
    return self.user.username